Core
=====================================================================


Contenu du core
---------------------------------------------------------------------

Fichiers importés dans le core :

```scss 
// module npm
// start
@import "@pidila/scampi/core/settings";
@import "@pidila/scampi/core/mixins";

// core
@import "@pidila/scampi/core/basics";

// end
@import "@pidila/scampi/core/helpers";
```

```scss
// submodule git
// start
@import "../../scampi/core/settings";
@import "../../scampi/core/mixins";

// core
@import "../../scampi/core/basics";

// end
@import "../../scampi/core/helpers";
```

- **settings.scss** : configurations et variables utilisées dans les fichiers du core ou communes à plusieurs modules.
- **mixins** : mixins transverses utilisés dans le core ou communs à plusieurs modules.
- **basics** : `normalize.scss`, `normalize-plus` et `generic.scss` pour poser un socle harmonisé entre tous les navigateurs et styler par défaut les tags html.
- **helpers** : ensemble de classes à importer à la fin de la liste de tous les imports.

Ces fichiers sont détaillés et commentés dans les pages [basics](core-basics.html), [helpers](core-helpers.html), [mixins](core-mixins.html) et [settings](core-settings.html). 
