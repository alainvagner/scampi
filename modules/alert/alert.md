# Alert


Présentation
-------------

Messages d'alerte courants : information, succès, alerte, danger/erreur. Les blocs d'alerte sont dotés d'une classe commune `alert` et d'une classe spécifique (ex. `alert-success`).

L'attribut `role="alert"` déclenche sa lecture immédiate au chargement de la page par les aides techniques. Pour cette raison, nous choisissons de ne pas mettre ce rôle sur les messages d'information.

Note : ce module est issu du framework Bootstrap.

Utilisation
-------------

Si vous utilisez ce composant pour afficher des messages de façon dynamique (apparition suite à l'activation d'un bouton ou la validation d'un formulaire sans rechargement de page par exemple) et que vous souhaitez faire vocaliser le contenu de ces messages par les lecteurs d'écran sans devoir déplacer le focus sur le message il faudra :

* Ajouter un `role="alert"` sur la `<div class="alert alert-xxx">`.
* Prévoir que la `<div class="alert alert-xxx" role="alert">` soit présente dans le code source par défaut mais vide.
* Générer le message à l'intérieur via javascript lorsque nécessaire.

### Configuration

Les variables proposées dans ce module sont :

#### Alerte
- `$alert-padding` : padding du bloc d'alerte, sa valeur par défaut est `($spacer / 2) $spacer`;
- `$alert-border-radius` : border-radius du bloc d'alerte, sa valeur par défaut est `$border-radius`;
- `$alert-link-font-weight` : font-weight du bloc d'alerte, sa valeur par défaut est `bold`;
- `$alert-border-width` : border-width du bloc d'alerte, sa valeur par défaut est `$border-width`;

#### Succès
- `$alert-success-bg` : couleur de fond, sa valeur par défaut est `$state-success-bg`;
- `$alert-success-text` : couleur du texte, sa valeur par défaut est `$state-success-text`;
- `$alert-success-border` : couleur de la bordure, sa valeur par défaut est `$state-success-border`;

#### Info
- `$alert-info-bg` : couleur de fond, sa valeur par défaut est `$state-info-bg`;
- `$alert-info-text` : couleur du texte, sa valeur par défaut est `$state-info-text`;
- `$alert-info-border` : couleur de la bordure, sa valeur par défaut est `$state-info-border`;

#### Warning
- `$alert-warning-bg` : couleur de fond, sa valeur par défaut est `$state-warning-bg`;
- `$alert-warning-text` : couleur du texte, sa valeur par défaut est `$state-warning-text`;
- `$alert-warning-border` : couleur de la bordure, sa valeur par défaut est `$state-warning-border`;

#### Danger
- `$alert-danger-bg` : couleur de fond, sa valeur par défaut est `$state-danger-bg`;
- `$alert-danger-text` : couleur du texte, sa valeur par défaut est `$state-danger-text`;
- `$alert-danger-border` : couleur de la bordure, sa valeur par défaut est `$state-danger-border`;

#### Emergency
- `$alert-emergency-bg` : couleur de fond, sa valeur par défaut est `$state-emergency-bg`;
- `$alert-emergency-text` : couleur du texte, sa valeur par défaut est `$state-emergency-text`;
- `$alert-emergency-border` : couleur de la bordure, sa valeur par défaut est `$state-emergency-border`;


Exemples d'utilisation
-------------

```html
<div class="alert alert-success" role="alert">
  <p class="alert-content"><strong>Bravo ! </strong>Votre compte a bien été créé.</p>
</div>

<div class="alert alert-info">
  <p class="alert-content"><strong>Votre messagerie : </strong>Vous avez 123 nouveaux messages privés. <a href="#">Accédez à votre boîte de messages</a>.</p>
</div>

<div class="alert alert-warning" role="alert">
  <p class="alert-content"><strong>Attention ! </strong> Votre porte-documents atteindra bientôt la limite autorisée.</p>
</div>

<div class="alert alert-danger" role="alert">
  <div class="alert-content">
    <p><strong>Erreur :-(</strong></p>
    <p>Le formulaire est incomplet, il contient 2 erreur(s) : </p>
    <ul>
      <li>L’adresse électronique est incorrecte.</li>
      <li>Le mot de passe est incorrect.</li>
    </ul>
  </div>
</div>

<div class="alert alert-emergency" role="alert">
  <svg class="svg-icon icon-phone" aria-hidden="true" focusable="false">
    <use xlink:href="/assets/icones/icon-sprite.svg#phone"></use>
  </svg>
  <div class="alert-content">
    <p><strong>En cas d'urgence</strong>, composez le <a href="#">18</a>.</p>
  </div>
</div>
```
