# Modal

Présentation
------------------------------------------------------------------------------

Le module modal de Scampi est largement basé sur celui de [Van11y](https://van11y.net/accessible-modal/) auquel lui a été ajouté quelques autres modifications pour une pleine conformité RGAA.



Utilisation
------------------------------------------------------------------------------

Le bouton appelant la modale porte la classe `js-modal` et les attributs `data-modal-close-text="×" data-modal-close-title="Fermer"` et  `data-modal-content-id="exempleModal"` où `exempleModal` est l’id de la fenêtre modale appelée.

Note : le cas sans js n'a pas encore été traité, cela fera l'objet d'une prochaine mise à jour.

### Configuration

Les variables proposées dans ce module sont :

- `$zindex-modal` : z-index de la modale, sa valeur par défaut est `1050`
- `$zindex-modal-background` : z-index du fond de la modale, sa valeur par défaut est `1040`
- `$modal-inner-padding` : padding de la modale, sa valeur par défaut est `1em`
- `$modal-title-padding` : padding du titre de la modale, sa valeur par défaut est `1rem`
- `$modal-title-line-height` : hauteur de ligne du titre, sa valeur par défaut est `$line-height-sm`
- `$modal-content-bg` : couleur de fond de la modale, sa valeur par défaut est `#fff`
- `$modal-content-border-color` : couleur de la bordure de la modale, sa valeur par défaut est `rgba(0,0,0,.2)`
//** Modal backdrop background color
- `$modal-backdrop-bg` : couleur de l'arrière plan de la modale, sa valeur par défaut est `#000`
- `$modal-backdrop-opacity` : opacité de l'arrière plan de la modale, sa valeur par défaut est `.5`
- `$modal-header-border-color` : bordure du header de la modale, sa valeur par défaut est `#e5e5e5`
- `$modal-footer-border-color` : bordure du footer de la modale, sa valeur par défaut est `$modal-header-border-color`
- `$modal-lg` : taille de la modale en affichage desktop, sa valeur par défaut est `56em`
- `$modal-md` : taille de la modale en affichage tablette, sa valeur par défaut est `37em`
- `$modal-sm` : taille de la modale en affichage mobile, sa valeur par défaut est `19em`
- `$modal-close-font-size` : taille du texte du bouton fermer, sa valeur par défaut est `$font-size-base * 1.5`
- `$modal-close-font-weight` : font-weight du bouton fermer, sa valeur par défaut est `bold`
- `$modal-close-color` : couleur du bouton fermer, sa valeur par défaut est `#000`
- `$modal-close-padding` : padding du bouton fermer, sa valeur par défaut est `0.1em 0.3em`

### Accessibilité

Respect du design pattern https://www.w3.org/TR/wai-aria-practices-1.1/#dialog_modal

- focus sur le premier élément tabulable
- `tab` et `shift-tab` ne se déplacent pas en dehors de la fenetre modale
- fermeture avec `Esc` ou au clic à l'extérieur de la fenêtre

### Script associé

Pour que ce module fonctionne, le script associé doit être appelé dans le pied de page, avant la fermeture du `body`.


Exemple d’utilisation
------------------------------------------------------------------------------

```html
<button class="btn btn-primary js-modal" data-modal-content-id="exempleModal" data-modal-close-text="×" data-modal-close-title="Fermer">Hop la modale !</button>

<div class="modal-template" id="exempleModal">
  <div class="modal-header">
    <h5 id="modal-title" class="modal-title" id="exampleModalLabel">Titre de la modale</h5>
  </div>
  <div class="modal-body">
    <p>Contenu de la modale</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At consequatur ratione similique ipsa rem ab dicta quaerat voluptate rerum asperiores temporibus illum qui explicabo molestias, quas harum. Dolor, magni, labore.</p>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary js-modal-close">Annuler</button>
    <button type="button" class="btn btn-primary">Continuer</button>
  </div>
</div>

```
