# Utiliser un module

Cette page présente la méthodologie de travail pour utiliser un module et la liste des modules disponibles avec une brève présentation de chacun d'entre eux.

Le répertoire de chaque module contient un fichier qui le présente et donne des exemples d'utilisation et le code html minimal nécessaire.


Comment utiliser un module ?
----------------------------

Deux voies vous sont offertes (nous recommandons la seconde).

### Modification directe

Importez le module et opérez directement dans celui-ci les modifications ou adaptations que vous souhaitez y effectuer.

Attention, cette méthode est la plus simple à mettre en œuvre mais ne permettra pas de bénéficier de mises à jour ou enrichissements éventuels des modules.

### Personnalisations distinctes

(L'ordre de cette liste est important.)

1. Créez un fichier _mon-module.scss dans le répertoire de votre projet.
2. Commencez par définir les éventuelles nouvelles variables ou valeurs de variables (sans les faire suivre de `!default`).
3. Importez le module originel de Scampi
4. Ajoutez ensuite vos éventuels propres mixins.
5. Ajoutez enfin vos propres règles de style.

#### Exemple

````scss
// personnalisation du module de blockquote
// fichier styles/mon-projet/mes-modules/_blockquote.scss

// module blockquote

// variables projet
$blockquote-border-color: $primary-color;

// import scampi (module npm)
@import "@pidila/scampi/modules/blockquote/index";
// import scampi (submodule git)
//@import "../../scampi/modules/blockquote/index";

// mixins projet

// styles projet
.blockquote {
  @include blockquote($font-size: 1em, $font-style: italic);

  padding-bottom: 0;
  padding-top: 0;

  p {
    margin-bottom: .5em;
  }

  .blockquote-footer {
    font-style: normal;
  }
}

````


Modules disponibles
-------------------

Les liens renvoient vers la page de documentation et démonstration de chaque module.

<div class="table">

| nom | Description |
| :-- | :---------- |
| [adobe-blank](https://pidila.gitlab.io/scampi/documentation/adobe-blank.html) | À ajouter en complément du module fonticon pour cacher des alternatives aux icônes si elles sont utilisées en fonte d'icônes. |
| [alert](https://pidila.gitlab.io/scampi/documentation/alert.html) | Messages d'alerte (information, succès, warning, danger/erreur).|
| [anchor-focus](https://pidila.gitlab.io/scampi/documentation/anchor-focus.html) | Corrige un bug de focus sur Chrome/Safari/IE.|
| [blockquote](https://pidila.gitlab.io/scampi/documentation/blockquote.html) | Blocs de citation. |
| [breadcrumb](https://pidila.gitlab.io/scampi/documentation/breadcrumb.html) | Fil d'ariane |
| [browsehappy](https://pidila.gitlab.io/scampi/documentation/browsehappy.html) | Affichage et styles d'un bloc recommandant la mise à jour des versions obsolètes d'Internet Explorer. |
| [buttons](https://pidila.gitlab.io/scampi/documentation/buttons.html) | Boutons et groupes de boutons. |
| [collapse](https://pidila.gitlab.io/scampi/documentation/collapse.html) | Affichage/Masquage de blocs de contenu. |
| [fontface](https://pidila.gitlab.io/scampi/documentation/fontface.html) | Gestion des webfonts. |
| [fonticon](https://pidila.gitlab.io/scampi/documentation/fonticon.html) | Règles pour la mise en œuvre de fonte d'icônes ; à utiliser en parallèle avec le module adobe-blank pour l'accessibilité. |
| [forms](https://pidila.gitlab.io/scampi/documentation/forms.html) | Formulaires. Complétable par les sous-modules [forms-error](https://pidila.gitlab.io/scampi/documentation/forms-error.html), [forms-inline](https://pidila.gitlab.io/scampi/documentation/forms-inline.html), [forms-input-group](https://pidila.gitlab.io/scampi/documentation/forms-input-group.html), [forms-required](https://pidila.gitlab.io/scampi/documentation/forms-required.html), [forms-fieldset-discrete](https://pidila.gitlab.io/scampi/documentation/forms-fieldset-discrete.html) et [forms-size](https://pidila.gitlab.io/scampi/documentation/forms-size.html). |
| [menu-simple](https://pidila.gitlab.io/scampi/documentation/menu-simple.html) | Version basique d'un menu horizontal responsive à un seul niveau. |
| [modal](https://pidila.gitlab.io/scampi/documentation/modal.html) | Fenêtres modales. |
| [pagination](https://pidila.gitlab.io/scampi/documentation/pagination.html) | Affichage de la pagination. |
| [rwd-utils](https://pidila.gitlab.io/scampi/documentation/rwd-utils.html) | Fonctions, mixins et styles utilitaires pour le responsive. |
| [select-a11y](https://pidila.gitlab.io/scampi/documentation/select-a11y.html) | Module qui transforme un élément select (multiple ou non) en liste de suggestions avec champ de recherche à l'intérieur de cette liste. |
| [site-banner-simple](https://pidila.gitlab.io/scampi/documentation/site-banner-simple.html) | Version basique d'un entête de site avec logo et nom du site (peut servir de base pour un placement de marianne). |
| [skip-link](https://pidila.gitlab.io/scampi/documentation/skip-link.html) | Liens d'évitement, aussi appelés liens d'accès rapides. |
| [svg-icons](https://pidila.gitlab.io/scampi/documentation/svg-icons.html) | Permet l'utilisation de sprite d'icônes au format svg. |
| [tables](https://pidila.gitlab.io/scampi/documentation/tables.html) | Tableaux (dont responsive). |
| [textarea-counter](https://pidila.gitlab.io/scampi/documentation/textarea-counter.html) | Affiche le nombre de caractères restant lorsque le champ de saisie est limité. |
| [u-comments](https://pidila.gitlab.io/scampi/documentation/u-comments.html) | Afficher/maquer des "post-it" de commentaires pendant la phase de développement. |
| [u-debug](https://pidila.gitlab.io/scampi/documentation/u-debug.html) | Divers utilitaires de debug : points de rupture, maps, rythme horizontal. |
| [u-palette](https://pidila.gitlab.io/scampi/documentation/u-palette.html) | Aide à la création de la palette des couleurs principales d'un site en fonction des valeurs renseignées dans les settings du projet. |

</div>
