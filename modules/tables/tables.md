# Tables

Présentation
---------------------------------------------------------------------

Ce module vise à présenter des modèles html de tableaux de données accessibles et apporter quelques règles de présentation à compléter.

Note : ce composant s'inspire de son homologue de Bootstrap.

Accessibilité
---------------------------------------------------------------------

Pour être pleinement accessible, un tableau de données doit comporter les éléments suivants :
- une balise `<caption>` contenant le titre ou le résumé selon que le tableau de données est simple ou complexe.
- des balises `<th>` pour identifier clairement les cellules d'en-tête
- des attributs `scope` pour lier les cellules de données aux cellules d'en-tête.

S'il s'agit d'un tableau de mise en forme, veiller à sa bonne linéarisation et ajouter un attribut `role="presentation"` sur la balise `<table>`.


Utilisation
---------------------------------------------------------------------

Ajouter la class `table` à tous les tableaux de données. Quelques classes additionnelles peuvent être ajoutées :
- `.table-striped`: ajouter un léger fond une ligne sur deux, 
- `.table-hover` : ajouter un effet au survol, 

En affichage mobile, un ascenseur horizontal apparaît.

### Configuration scss

Variables scss proposées dans ce module :

- `$table-cell-padding` : padding des cellules, par défaut `.75em .4rem`
- `$table-color` : couleur du texte, sa valeur par défaut est `$body-color`
- `$table-bg` : couleur de fond, sa valeur par défaut est `transparent`
- `$table-accent-bg` : couleur de fond du tableau strié, sa valeur par défaut est `$gray-9`
- `$table-hover-color` : couleur du texte au survol, sa valeur par défaut est `$table-color`
- `$table-hover-bg` : couleur de fond au survol, sa valeur par défaut est `$gray-10`
- `$table-border-width` : bordure, sa valeur par défaut est `$border-width`
- `$table-border-color` : couleur de la bordure, sa valeur par défaut est `$gray-9`
- `$table-head-bg` : couleur de fond de l'entête, sa valeur par défaut est `$gray-9`
- `$table-head-color` : couleur du texte de l'entête, sa valeur par défaut est `$body-color`
- `$table-caption-color` : couleur du texte du titre ou du résumé, sa valeur par défaut est `$body-color`

Exemples d’utilisation
---------------------------------------------------------------------

``` html
<h3>Tableau simple</h3>
<table class="table table-striped table-hover">
 <caption>Palette Pidila</caption>
 <tr class="thead">
  <th scope="col">Nom variable</th>
  <th scope="col">Valeur hexa</th>
  <th scope="col">Usage</th>
 </tr>
 <tr>
  <td>$body-color</td>
  <td>#363740</td>
  <td>Texte courant</td>
 </tr>
 <tr>
  <td>$primary-color</td>
  <td>#2b2e4a</td>
  <td>Fond boutons d’action principale</td>
 </tr>
 <tr>
  <td>$secondary-color</td>
  <td>#da3b39</td>
  <td>Titres h1, h2, h3</td>
 </tr>
 <tr>
  <td>$piyellow</td>
  <td>#f0c825</td>
  <td>Passages de code sur fond sombre</td>
 </tr>
</table>
```
