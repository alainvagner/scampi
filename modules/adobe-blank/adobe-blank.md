# Adobe-blank

Présentation
-------------

Note : ce module vise notamment à être utilisé conjointement au module [fonticon](fonticon.html).

Une [fonte créée par Adobe](https://github.com/adobe-fonts/adobe-blank
) qui n’affiche rien mais dont le texte sera restitué par les aides techniques de lecture d’écran. On l’utilise pour offrir une alternative textuelle aux icônes utilisées via une fonte d’icones.

Utilisation
-----------

### Configuration

Pour mettre en œuvre ce module vous devez passer le setting `$enable-adobe-blank` à `true`et affecter la class `blank` à l'élément que vous voulez masquer. Par défaut, la valeur de `$enable-adobe-blank` est à `false`.

Exemples d’utilisation
---------------------

```` html
<p><a href="#">
  <span class="icon icon-download" aria-hidden="true"></span> 
  Rapport d'activité 
  <span class="blank">Télécharger</span>
</a></p>

<p><a href="#">
  Un lien externe 
  <span class="blank">- Nouvelle fenêtre</span>
  <span class="icon icon-external" aria-hidden="true"></span>
</a></p>
````

À noter
-------

Pour les icônes le Pôle intégration html de la DILA utilise plus volontiers un sprite SVG et le module [svg-icons](svg-icons.html) associé à du texte masqué par la [class sr-only](core-mixins.html#sr-only).

