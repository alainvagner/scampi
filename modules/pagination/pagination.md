# Pagination

Présentation
-----------------------

Module de pagination : division de l’affichage d’une page de résultats pour une recherche.

Il s’agit d’une pagination simple présentant par exemple les numéros des pages de résultats, avec, aux deux extrémités de cette liste, des liens permettant de naviguer vers la page précédente ou la page suivante. Si le nombre de pages est supérieur à 5, on peut placer un élément intermédiaire "...".

Utilisation
-------------------------

### Configuration

Les variables proposées dans ce module sont :

- `$zindex-modal` : z-index de la modale, sa valeur par défaut est `1050`
- `$zindex-modal-background` : z-index du fond de la modale, sa valeur par défaut est `1040`
- `$page-link-color` : couleur des liens, sa valeur par défaut est `$body-color`
- `$page-link-bg` : couleur de fond des liens, sa valeur par défaut est `transparent`
- `$page-link-border` : couleur de la bordure des liens, sa valeur par défaut est `$primary-color`
- `$page-link-color-focus` : couleur des liens au focus, sa valeur par défaut est `#fff`
- `$page-link-bg-focus` : couleur de fond des liens au focus, sa valeur par défaut est `$primary-color`
- `$page-link-active-color` : couleur des liens actifs, sa valeur par défaut est `$page-link-color-focus`
- `$page-link-active-bg` : couleur de fond des liens actifs, sa valeur par défaut est `$page-link-bg-focus`
- `$page-item-prev-icon` : icône précédente, sa valeur par défaut est `"\2039"`
- `$page-item-next-icon` : icône suivante, sa valeur par défaut est `"\203A"`


### Accessibilité

Placer des attributs :

* sur l’élément nav (aria-label="Pagination") ;
* sur le lien conduisant vers la page précédente (title="page précédente") ;
* sur le lien conduisant vers la page suivante (title="page suivante") ;
* sur les liens conduisant à chaque page (title="page xxx").

Placer un texte caché sur l’item de la page active, grâce à la classe "sr-only".

### Responsive

Sur petit écran, les liens conduisant vers la page précédente ou la page suivante n’affichent que le chevron, sans l’intitulé (qui est restitué par les synthèses vocales grâce au mixin sr-only).

### À noter

Lors de l’intégration réelle, le lien "page précédente" doit être désactivé si l’on est sur la première page et le lien "page suivante" doit être désactivé si l’on est sur la dernière page.

Si l'on veut insérer une icône avec before ou after, il faudra placer l'icone dans un span portant l'attribut aria-hidden="true".


Exemple d’utilisation
------------------------------------------------------------------------------

```` html
<nav aria-label="page navigation" role="navigation">
  <ul class="pagination">
    <li class="page-item page-item-prev">
      <a class="page-link" href="#" title="page précédente">
        <span class="page-item-icon" aria-hidden="true"></span>
        <span>Précédent</span>
      </a>
    </li>
    <li class="page-item"><a class="page-link" href="#" title="page 1">1</a></li>
    <li class="page-item active"><b class="page-link">2<span class="sr-only"> page active</span></b></li>
    <li class="page-item"><a class="page-link" href="#" title="page 3">3</a></li>
    <li class="page-item"><a class="page-link" href="#" title="page 4">4</a></li>
    <li class="page-item"><a class="page-link" href="#" title="page 5">5</a></li>
    <li class="page-item page-item-next">
      <a class="page-link" href="#" title="page suivante">
        <span>Suivant</span>
        <span class="page-item-icon" aria-hidden="true"></span>
      </a>
    </li>
  </ul>
</nav>

````
