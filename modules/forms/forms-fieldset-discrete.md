# Forms fieldset-discrete


Présentation
---------------------------------------------------------------------

Le sous-module `forms-fieldset-discrete.scss` permet d'afficher un fieldset sans les bordures ni les paddings. Il fait partie du module <a href="forms.html">forms</a>.


Utilisation
---------------------------------------------------------------------

Donner la class `.fieldset-discrete` à l'élément fieldset.

Exemple
---------------------------------------------------------------------

Styles appliqués par le fichier `forms/_style-forms-fieldset-discrete.scss`.

```html
<form>
  <fieldset class="form-group fieldset-discrete">
    <legend>Choix multiples avec fieldset non apparent (.fieldset-discrete)</legend>

    <div class="form-check">
      <label for="checkbox_08" class="form-check-label">
        <input class="form-check-input" type="checkbox" id="checkbox_08">
        choix 1
      </label>
    </div>
    <div class="form-check">
      <label for="checkbox_09" class="form-check-label">
        <input class="form-check-input" type="checkbox" id="checkbox_09">
        choix 2
      </label>
    </div>
    <div class="form-check">
      <label for="checkbox_10" class="form-check-label">
        <input class="form-check-input" type="checkbox" id="checkbox_10">
        choix 3
      </label>
    </div>
  </fieldset>
</form>
```
