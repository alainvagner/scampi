# Forms


Présentation
---------------------------------------------------------------------

Le module forms de Scampi reprend l’essentiel du code du module [forms de Bootstrap 4](https://getbootstrap.com/docs/4.1/components/forms/) et l'enrichit.

Ce qui n’est pas repris :

- form-grid (Scampi n'embarque pas de système de grille pour laisser le choix à l'utilisateur de son système préféré)
- form-plaintext 
- custom-form 

Ce qui est ajouté :

- forms-required : pour afficher la mention d'un champ obligatoire
- forms-fieldset-discrete : pour supprimer les présentations standards des fieldsets 
- forms-error : pour afficher les erreurs en contexte

Le module est composé d'un fichier maître (`_forms.scss`) dans lequel sont définies toutes les variables et où l'on peut commenter ou décommenter l'import de styles complémentaires selon les besoins du projet. En tout état de cause le partial `_styles-forms.scss` est indispensable.

Utilisation
---------------------------------------------------------------------

La présentation des éléments de formulaire passe presque uniquement par la personnalisation des variables et l’ajout de classes sur les éléments html.

### Configuration

Les variables proposées dans ce module sont :

- `$input-btn-border-width` : bordure des boutons, sa valeur par défaut est `$border-width`;
- `$input-padding-x` : espacement horizontal des input, sa valeur par défaut est `.75em`;
- `$input-padding-y` : espacement vertical des input, sa valeur par défaut est `.5em`;
- `$input-bg` : couleur de fond des input, sa valeur par défaut est `#fff`;
- `$input-color` : couleur du texte des input, sa valeur par défaut est `$gray`;
- `$input-border-color` : couleur de la bordure des input, sa valeur par défaut est `#ccc`;
- `$input-box-shadow` : ombrage des input, sa valeur par défaut est `inset 0 1px 1px rgba(0,0,0,.075)`;
- `$input-border-radius` : border-radius des input, sa valeur par défaut est `$border-radius`;
- `$input-placeholder-color` : couleur du placeholder, sa valeur par défaut est `#999 !default`;
- `$input-height` : hauteur de l'input, sa valeur par défaut est `(($font-size-base * $line-height) + ($input-padding-y * 2))`;

De nombreuses autres variables personnalisables sont décrites dans le fichier [modules/forms/_index.scss](https://gitlab.com/pidila/scampi/blob/master/modules/forms/_index.scss).

Documentation des feuilles de style complétant `_styles-forms.scss` :

<ul>
    <li><a href="forms-error.html">forms-error</a> : nos ajouts au module de Bootstrap pour l'affichage des messages d'erreur globaux ou en contexte</li>
    <li><a href="forms-inline.html">forms-inline</a> : affichage de champs en ligne</li>
    <li><a href="forms-input-group.html">forms-input-group</a> : affichage de champs groupés</li>
    <li><a href="forms-required.html">forms-required</a> : ajout au module de Bootstrap pour afficher la mention d'un champ obligatoire</li>
    <li><a href="forms-fieldset-discrete.html">forms-fieldset-discrete</a> : ajout au module de Bootstrap pour afficher un fieldset sans bordures</li>
    <li><a href="forms-size.html">forms-size</a> : mise à dispo de variations des tailles</li>
</ul>


Exemples pour les éléments de base
---------------------------------------------------------------------

Styles appliqués par le fichier `forms/_style-forms.scss`.

### Input text
```html
<div class="form-group">
  <label for="nom">Nom</label>
  <input type="text" class="form-control" id="nom">
</div>
```

### Input email
```html
<div class="form-group">
  <label for="email">Adresse electronique (ex. : nom.prenom@example.org)</label>
  <input type="email" class="form-control" id="email">
</div>
```

### Input file
```html
<div class="form-group">
  <label for="exampleFormControlFile1">File input</label>
  <input type="file" class="form-control-file" id="exampleFormControlFile1">
</div>
```

### Range input
```html
<div class="form-group">
  <label for="formControlRange">Range input</label>
  <input type="range" class="form-control-range" id="formControlRange">
</div>
```

### Texte d'aide
```html
<div class="form-group">
  <label for="inputPassword5">Mot de passe</label>
  <input type="password" id="inputPassword5" class="form-control" aria-describedby="passwordHelpBlock">
  <small id="passwordHelpBlock" class="form-text text-muted">
    Le mot de passe doit comporter un minimum de 8 caractères avec : au moins une lettre en majuscule, au moins une lettre en minuscule, au moins un chiffre.
  </small>
</div>
```

### Lecture seule
L’attribut `readonly` empeche la modification du champ par l’utilisateur.

Si une valeur doit être affichée, il faut la placer dans l’attribut `value`.

```html
<div class="form-group">
  <input class="form-control" type="text" readonly value="Input text en lecture seule">
</div>
```

### Checkboxes
```html
<fieldset class="form-group">
  <legend>Choix multiples</legend>
  <div class="form-check">
    <label for="checkbox_01" class="form-check-label">
      <input class="form-check-input" type="checkbox" id="checkbox_01">
      choix 1
    </label>
  </div>
  <div class="form-check">
    <label for="checkbox_02" class="form-check-label">
      <input class="form-check-input" type="checkbox" id="checkbox_02">
      choix 2
    </label>
  </div>
  <div class="form-check">
    <label for="checkbox_03" class="form-check-label">
      <input class="form-check-input" type="checkbox" id="checkbox_03">
      choix 3
    </label>
  </div>
  <div class="form-check disabled">
    <label for="checkbox_04" class="form-check-label">
      <input type="checkbox" class="form-check-input" disabled id="checkbox_04">
      Option non activable
    </label>
  </div>
</fieldset>

<fieldset class="form-group">
  <legend>Choix multiples (inline)</legend>
  <label for="checkbox_05" class="form-check-inline">
    <input class="form-check-input" type="checkbox" value="" id="checkbox_05">
    choix 1
  </label>
  <label for="checkbox_06" class="form-check-inline">
    <input class="form-check-input" type="checkbox" value="" id="checkbox_06">
    choix 2
  </label>
  <label for="checkbox_07" class="form-check-inline">
    <input class="form-check-input" type="checkbox" value="" id="checkbox_07">
    choix 3
  </label>
</fieldset>

<div class="form-check">
  <label for="checkbox_11" class="form-check-label">
    <input type="checkbox" class="form-check-input" required id="checkbox_11">
    J’ai bien lu <a href="#">les conditions</a>, je coche avant d’envoyer.
  </label>
</div>
```

### Boutons Radio
```html
<fieldset class="form-group">
  <legend>Choix unique</legend>
  <div class="form-check">
    <label for="radio_01" class="form-check-label">
      <input class="form-check-input" type="radio" name="choix-unique" value="value-1" id="radio_01">
      choix 1
    </label>
  </div>
  <div class="form-check">
    <label for="radio_02" class="form-check-label">
      <input class="form-check-input" type="radio" name="choix-unique" value="value-2" id="radio_02">
      choix 2
    </label>
  </div>
  <div class="form-check">
    <label for="radio_03" class="form-check-label">
      <input class="form-check-input" type="radio" name="choix-unique" value="value-3" id="radio_03">
      choix 3
    </label>
  </div>
  <div class="form-check disabled">
    <label for="radio_04" class="form-check-label">
      <input class="form-check-input" type="radio" name="choix-unique" value="value-4" disabled id="radio_04">
      Option non activable
    </label>
  </div>
</fieldset>

<fieldset class="form-group">
  <legend>Choix unique (inline)</legend>
  <label for="radio_08" class="form-check-inline">
    <input class="form-check-input" type="radio" name="choix-unique-2" value="value-1" id="radio_08">
    choix 1
  </label>
  <label for="radio_09" class="form-check-inline">
    <input class="form-check-input" type="radio" name="choix-unique-2" value="value-2" id="radio_09">
    choix 2
  </label>
  <label for="radio_10" class="form-check-inline">
    <input class="form-check-input" type="radio" name="choix-unique-2" value="value-3" id="radio_10">
    choix 3
  </label>
</fieldset>
```

### Textarea
```html
<div class="form-group">
  <label for="exampleFormControlTextarea1">Textarea</label>
  <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
</div>
```

### Select
```html
<div class="form-group">
  <label for="select">Select</label>
  <select class="form-control" id="select">
    <option>1</option>
    <option>2</option>
    <option>3</option>
    <option>4</option>
    <option>5</option>
  </select>
</div>
```

### Select avec optgroup
```html
<div class="form-group">
  <label for="select_element_multi">Select à groupes d’options</label>
  <select id="select_element_multi" class="form-control">
    <option label="non défini" selected disabled>Choisir</option>
    <optgroup label="Option Group 1">
      <option value="1">Option 1.1</option>
      <option value="2">Option 1.2</option>
      <option value="3">Option 1.3</option>
      <option value="4">Option 1.4</option>
    </optgroup>
    <optgroup label="Option Group 2">
      <option value="1">Option 2.1</option>
      <option value="2">Option 2.2</option>
      <option value="3">Option 2.3</option>
      <option value="4">Option 2.4</option>
    </optgroup>
  </select>
</div>
```

### Select multiple
```html
<div class="form-group">
  <label for="selectMultiple">Select multiple</label>
  <select multiple class="form-control" id="selectMultiple">
    <option>1</option>
    <option>2</option>
    <option>3</option>
    <option>4</option>
    <option>5</option>
  </select>
</div>
```
