# Forms-input-group


Présentation
--------------------------------------------------------------------

Le sous-module `forms-input-group` permet de réunir visuellement les éléments de formulaire. Il fait partie du module <a href="forms.html">forms</a>. On l'utilise par exemple pour coller une loupe à un champ de recherche.

Dépendances
--------------------------------------------------------------------

À noter : forms-input-group nécessite l’utilisation du module de base de <a href="buttons.html">buttons</a> ainsi que de son sous-module <a href="buttons-group.html">buttons-group</a>.


Utilisation
--------------------------------------------------------------------

Donner la class `input-group` au bloc qui contient l'input et le bouton à coller.


Exemples
--------------------------------------------------------------------

Styles appliqués par le fichier `forms/_style-forms-input-group.scss`.

```html
<form>
  <p class="input-group">
    <div class="input-group-prepend">
      <span class="input-group-text" id="basic-addon1">@</span>
    </div>
    <input type="text" class="form-control" placeholder="Pseudo" aria-label="Pseudo" aria-describedby="basic-addon1">
  </p>

  <p class="input-group">
    <input type="text" class="form-control" placeholder="Nom d'utilisateur" aria-label="Nom d'utilisateur" aria-describedby="basic-addon2">
    <div class="input-group-append">
      <span class="input-group-text" id="basic-addon2">@exemple.com</span>
    </div>
  </p>

  <label for="basic-url">Votre site web</label>
  <div class="input-group">
    <div class="input-group-prepend">
      <span class="input-group-text" id="basic-addon3">https://www.exemple.com/</span>
    </div>
    <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3">
  </div>

  <div class="input-group">
    <div class="input-group-prepend">
      <span class="input-group-text">€</span>
    </div>
    <input type="text" class="form-control" aria-label="Arrondi (à l'euro supérieur)">
    <div class="input-group-append">
      <span class="input-group-text">.00</span>
    </div>
  </div>

  <div class="input-group">
    <div class="input-group-prepend">
      <span class="input-group-text">Avec textarea</span>
    </div>
    <textarea class="form-control" aria-label="Avec textarea"></textarea>
  </div>
</form>
```
