# Forms-inline


Présentation
---------------------------------------------------------------------

Le sous-module `forms-inline` permet d'afficher des éléments côte à côte. Il fait partie du module <a href="forms.html">forms</a>. 

Utilisation
---------------------------------------------------------------------

Placer la class `form-inline` sur le bloc contenant les éléments à distribuer en ligne.


Exemple
---------------------------------------------------------------------

Styles appliqués par le fichier `forms/_style-forms-inline.scss`.

```html
<h3>Inline forms</h3>
<div class="form-inline">
  <label class="sr-only" for="inlineFormInputName2">Nom</label>
  <input type="text" class="form-control" id="inlineFormInputName2" placeholder="Nom">
  <label class="sr-only" for="inlineFormInputGroupUsername2">Pseudo</label>
  <div class="input-group">
    <div class="input-group-prepend">
      <div class="input-group-text">@</div>
    </div>
    <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Pseudo">
  </div>

  <div class="form-check">
    <input class="form-check-input" type="checkbox" id="inlineFormCheck">
    <label class="form-check-label" for="inlineFormCheck">
      Cochez-moi
    </label>
  </div>
  <button type="submit" class="btn btn-primary">Envoyer</button>
</div>

```
