# Forms-required


Présentation
---------------------------------------------------------------------

Le sous-module `forms-required` permet d'indiquer à un utilisateur qu'un champ est requis. Il fait partie du module <a href="forms.html">forms</a>.


Utilisation
---------------------------------------------------------------------

La variable `$symbol-required` définit la couleur du symbole utilisé pour indiquer les champs obligatoires ; la mise en œuvre se fait en appliquant la class `.symbol-required` sur le span qui contient l’astérisque.

À noter : une phrase expliquant la signification du symbole et placée en début de formulaire est fortement recommandée.

Exemple
---------------------------------------------------------------------

Styles appliqués par le fichier `forms/_style-forms-required.scss`.

```html
<form>
  <p>Les champs marqués d’un <span class="symbol-required">* </span>sont obligatoires.</p>

  <div class="form-check">
    <label for="form-check-input" class="form-check-label">
      <input type="checkbox" class="form-check-input" id="form-check-input" required>
      <span class="symbol-required">* </span>J’accepte les <a href="#">conditions générales d’utilisation</a>
    </label>
  </div>
</form>
```
