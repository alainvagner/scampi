# Forms-error


Présentation
---------------------------------------------------------------------

Le sous-module forms-error fait partie du module <a href="forms.html">forms</a>. 

Le modèle html et les styles permettent de restituer au mieux les erreurs à l'utilisateur quel que soit son contexte de navigation.

Accessibilité
---------------------------------------------------------------------

Le lien entre le champ erroné et le message d'explication est fait par l'attribut aria-describedby qui prend pour valeur l'id du message d'erreur.

Utilisation
---------------------------------------------------------------------

L’affichage en contexte des erreurs de formulaire se fait en plaçant une class `has-error` sur la div englobant le label et l’input et en liant le message d'erreur avec un attribut aria-describedby sur l'input concerné.


Exemple
---------------------------------------------------------------------

Styles appliqués par le fichier `forms/_style-forms-error.scss`.

```html
<h3>Champ avec erreur</h3>
<div class="form-group has-error">
  <p id="error-lorem" class="error-desc"> Le champ <em>Lorem ipsum</em> est obligatoire.</p>
  <label for="lorem">Lorem ipsum&nbsp;<span class="symbol-required">*</span>
  </label>
  <input type="text" id="lorem" class="form-control" required aria-required="true" aria-describedby="error-lorem">
</div>
```
