# Buttons-link


Présentation
---------------------------------------------------------------------

Ce module est dépendant du module <a href="buttons.html">buttons</a>. Il permet de donner à un bouton l'aspect d'un lien.


Accessibilité
---------------------------------------------------------------------

Bien que nous mettions à disposition une classe permettant cette simulation de bouton en lien, nous recommandons fortement de n'y avoir recours que pour des cas où il existe des contraintes ne permettant pas d'autre solution. Il est bien préférable de donner à un élément le balisage sémantique correct.

Utilisation
---------------------------------------------------------------------

Ajouter la class "btn-link" à la class "btn" de l'élément button.


Exemple
---------------------------------------------------------------------

```html
<p><button type="button" class="btn btn-link">Bouton déguisé en lien</button></p>
<p><a href="#">Lien classique</a></p>
```
