# Buttons-block


Présentation
---------------------------------------------------------------------

Ce module est dépendant du module <a href="buttons.html">buttons</a>.

Il permet d'étendre un bouton sur toute la largeur disponible offerte par le parent. Cette classe peut également s'appliquer aux inputs type submit, reset ou button.


Utilisation
---------------------------------------------------------------------

Ajouter la class "btn-block" à la class "btn" sur l'élément button ou input.


Exemple
---------------------------------------------------------------------

```html
<h3>Boutons toute largeur</h3>

<h4>class="btn btn-block"</h4>
<button type="button" class="btn btn-block">Bouton toute largeur</button>

<h4>Class btn btn-block appliquées à un input type="submit" :</h4>
<input type="submit" class="btn btn-block" value="Valider" />

<p>Les classes peuvent être combinées entre elles. Exemples :</p>

<h4>class="btn btn-block btn-primary"</h4>
<button type="button" class="btn btn-block btn-primary">Bouton primaire toute largeur</button></p>

<h4>class="btn btn-block btn-lg"</h4>
<button type="button" class="btn btn-block btn-lg">Gros bouton toute largeur</button>
```
