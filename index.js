import anchorFocus from './modules/anchor-focus/index.js'
import collapse from './modules/collapse/index.js'
import menuSimple from './modules/menu-simple/index.js'
import modal from './modules/modal/index.js'
import selectA11y from './modules/select-a11y/index.js'
import skipLinks from './modules/skip-link/index.js'
import svgXuse from './modules/svg-icons/svgxuse.js'
import textareaCounter from './modules/textarea-counter/index.js'
import uComments from './modules/u-comments/index.js'
import uPalette from './modules/u-palette/index.js'

var scampi =  {};

scampi.init = function() {
    if(document.documentElement.classList.contains('no-js')){
        document.documentElement.classList.remove('no-js');
        document.documentElement.classList.add('js');
    }
}



export { scampi, anchorFocus, collapse, menuSimple, modal, selectA11y, skipLinks, svgXuse, textareaCounter, uComments, uPalette }
